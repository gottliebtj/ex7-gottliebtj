
/**
*Author: Tyler Gottlieb
*Date:11/10/18
*
*
*/
package step_definitions;

import cucumber.api.java8.En;
import cucumber.runtime.java8.LambdaGlueBase;
import cucumber.api.java8.En;
import cucumber.api.PendingException;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import static org.junit.Assert.*;


import pbook.*;

public class TestUndoRedo implements En {

  private String[] outputLinesFromMainFor(AbstractMain mainObject,
                                          String inputText, String[] commands) {
    ByteArrayOutputStream capture = new ByteArrayOutputStream();
    PrintStream ps = new PrintStream(capture);
    PrintStream savedSystemOutput = System.out;
    System.setOut(ps);

    InputStream is = null;
    try {
      is  = new ByteArrayInputStream(inputText.getBytes("UTF-8"));
    } catch (UnsupportedEncodingException e) {
      System.err.print("Fatal error: " + e);
      System.exit(1);
    }
    InputStream savedSystemInput = System.in;
    System.setIn(is);
    mainObject.abstractMain(commands);

    // Put things back
    System.out.flush();
    System.setOut(savedSystemOutput);
    System.setIn(savedSystemInput);

    String lines[] = capture.toString().split("[\r\n]+");

    return lines;
  }

  private String[] resultLines = null;
  private PhoneBook book = new PhoneBook();

  public TestUndoRedo() {
    ///////////////////////////////////////////////
///////////////////////////////////////////////
  Given("^I enter the ([a-zA-Z]*) command without anything to ([a-zA-Z]*)$", (String command, String command2) -> {
      String[] commands = new String[2];
      commands[0]=command;
      commands[1]=command2;
      resultLines = outputLinesFromMainFor(new Phones(),command,commands);
      });
  Then("^the system should respond with (.*)$", (String response) -> {
        assertTrue(resultLines.length > 0);
        assertEquals(response, resultLines[0]);
      });
    ///////////////////////////////////////////////
///////////////////////////////////////////////

Given("^I have a book with ([a-zA-Z]*), ([a-zA-Z]*), ([a-zA-Z]*) as users and ([0-9]*), ([0-9]*), ([0-9]*) as their numbers$", (String name1,String name2,String name3, String number1,String number2,String number3) -> {
  Entry entry = new Entry(name1,number1);
  Entry entry1 = new Entry(name2,number2);
  Entry entry2 = new Entry(name3,number3);
  book.add(entry);
  book.add(entry1);
  book.add(entry2);
  });
  When("^I ([a-z]*) the last add$", (String command) -> {
    String[] commands = new String[2];
    commands[0]=command;
    commands[1]=command;
  resultLines = outputLinesFromMainFor(new Phones(),command,commands);
  });
Then("^the system should exit silently.$", () -> {
    assertEquals("", resultLines[0]);
  });

///////////////////////////////////////////
///////////////////////////////////////
  Given("^I use the add command", () -> {
  Entry entry = new Entry("Tyler","85473892");
  book.add(entry);
  });
  Then("^the list should not be empty$", () -> {
  assertTrue(!book.isEmpty());
  });
  /////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////
  Given("^the name ([a-zA-Z]*) and number ([0-9]*) is recently deleted", (String name, String number) -> {
    Entry entry = new Entry(name,number);
    book.add(entry);
    });
    When("^the ([a-z]*) function is executed$", (String command) -> {
      String[] commands = new String[2];
      commands[0]=command;
      commands[1]=command;
    resultLines = outputLinesFromMainFor(new Phones(),command,commands);
    });
    Then("^the ([a-z-A-z]*) entry with number ([0-9]*) should be restored to the phone book.$", (String name, String expectedNumber) -> {
      Entry found =  book.lookup(name);
      assertEquals(found.name(),name);
      });
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
  Given("^I attempt to remove something$", () -> {
        //  book = new PhoneBook();
          //changed above this line
          Entry entry = new Entry("Tyler","85473892");
          book.add(entry);
          });
  When("^one item has been added$",() -> {
            book.remove("Tyler");

          });
  Then("^the list should be empty$", () -> {
          assertTrue(book.isEmpty());
          });

  }
}
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
