#
# checkout.feature: Cucumber script for cashier system
# Author: Tyler Gottlieb
#

Feature: checking it out

      Scenario Outline: redo something not there
        Given I enter the <command> command without anything to <command>
        Then the system should respond with <response>

        Examples:
         | command |response|
         | list | |
         | redo |No commands to redo.|


       Scenario Outline: Using add
          Given I use the <command> command
          Then the list should not be empty

          Examples:
           | command | Response|
           | add | Enter name to add:|

        Scenario Outline: Using remove
          Given I attempt to remove something
          When one item has been added
          Then the list should be empty

          Examples:
           | command |
           | remove |

         Scenario Outline: Undo something in a list
          Given I have a book with <name>, <name2>, <name3> as users and <number>, <number2>, <number3> as their numbers
          When I undo the last add
          Then the system should exit silently.

             Examples:
              | name |name2|name3| number | number2| number3 |
              | Tyler |Chris |Matt | 3948934|235514|2345114|
              | Kristina |Courtney |Chloe | 4562434|12345114|19423414|
          



            Scenario Outline: Redo something in a list
             Given the name <name> and number <number> is recently deleted
             When the redo function is executed
             Then the <name> entry with number <number> should be restored to the phone book.

                Examples:
                 | name | number |
                 | Tyler | 3948934|
                 | Sanchmo | 4562434|
                 | Kristina | 4562434|
                 | NAcho | 4562434|
