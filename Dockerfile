#
# DockerFile for exercise 7: cucumber with Java 8
#   Use: docker build ./
#

FROM java:8
# install cucumber, junit4
COPY install-cucumber-for-java /do-install
RUN bash /do-install
# switch to java 8
RUN update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
COPY . /container
# run tests
RUN cd /container; bash ./run-cucumber
